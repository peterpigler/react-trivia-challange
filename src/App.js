import { Route, useHistory, Redirect } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import { ThemeProvider } from "styled-components";
import { connect } from "react-redux";
import Home from "./containers/Home/Home";
import Results from "./containers/Results/Results";
import Game from "./containers/Game/Game";
import ThemeChanger from "./containers/ThemeChanger/ThemeChanger";
import { light, dark } from "./styles/theme";
import { GlobalStyles } from "./styles/global";
import * as actions from "./redux/actions";

const App = (props) => {
  const { theme, changeTheme } = props;
  const history = useHistory();
  const themeMap = {
    dark,
    light,
  };

  return (
    <>
      <ThemeProvider theme={themeMap[theme]}>
        <GlobalStyles />
        <ThemeChanger theme={theme} changeTheme={changeTheme} />
        <Route path="/" exact>
          <Home history={history} />
        </Route>
        <Route path="/results">
          <Results history={history} />
        </Route>
        <Route path="/play" exact>
          <Game history={history} />
        </Route>
        <Redirect to="/" />
      </ThemeProvider>
    </>
  );
};

const mapStateToProps = (state) => ({
  theme: state.theme,
});

const mapDispatchToProps = (dispatch) => ({
  changeTheme: (theme) => dispatch(actions.changeTheme(theme)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
