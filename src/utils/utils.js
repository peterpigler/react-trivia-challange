export const escapeString = (string) => {
  if (string) {
    return string
      .replace(/&quot;/g, '"')
      .replace(/&#039;/g, "'")
      .replace(/&ldquo;/g, "“")
      .replace(/&rdquo;/g, "”")
      .replace(/&ocirc;/g, "˚");
  }
  return string;
};

const answerMap = {
  True: true,
  False: false,
};

export const calcScoreArray = (questions, answers) =>
  questions.map((q, idx) => answers[idx] === answerMap[q.correct_answer]);

export const calcScore = (questions, answers) => {
  const scoreArray = calcScoreArray(questions, answers);
  return scoreArray.reduce((a, b) => a + b, 0);
};
