import { Button } from "react-bootstrap";
import "./style.css";

const ThemeChanger = (props) => {
  const { theme, changeTheme } = props;
  return (
    <div className="theme-changer">
      <Button
        type="checkbox"
        checked={theme === "dark"}
        onClick={() => changeTheme(theme === "light" ? "dark" : "light")}
      >
        Dark mode
      </Button>
    </div>
  );
};

export default ThemeChanger;
