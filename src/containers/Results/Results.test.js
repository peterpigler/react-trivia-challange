import { render, fireEvent } from "@testing-library/react";
import { createStore } from "redux";
import { Provider } from "react-redux";
import Results from "./Results";

const startingState = {
  currentQuestion: 0,
  questions: [
    { question: "baz", category: "foo category", correct_answer: "True" },
    { question: "bar", category: "bar category", correct_answer: "True" },
  ],
  answers: [true, false],
  theme: "light",
};

const reducer = (state = startingState, action) => {
  switch (action.type) {
    case "SET_CURRENT_QUESTION":
      return {
        ...state,
        currentQuestion: state.currentQuestion + 1,
      };
    default:
      return state;
  }
};

const renderWithRedux = (
  component,
  { initialState, store = createStore(reducer, initialState) } = {}
) => ({
  ...render(<Provider store={store}>{component}</Provider>),
});

it("can render with redux", () => {
  const { getByTestId } = renderWithRedux(<Results />);

  const scoreHeader = getByTestId("score-header");
  expect(scoreHeader).toHaveTextContent(/1 \/ 2/g);

  const badgeCorrect = getByTestId("question-0-badge");
  expect(badgeCorrect).toHaveTextContent("+");
  expect(badgeCorrect).toContainHTML("bg-success");

  const badgeIncorrect = getByTestId("question-1-badge");
  expect(badgeIncorrect).toHaveTextContent("-");
  expect(badgeIncorrect).toContainHTML("bg-danger");
});
