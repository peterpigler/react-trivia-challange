import React from "react";
import { connect } from "react-redux";
import { Badge, Card, Button } from "react-bootstrap";
import * as actions from "../../redux/actions";
import { escapeString, calcScore, calcScoreArray } from "../../utils/utils";
import "./style.css";

const Results = (props) => {
  const { answers, questions, playAgain, history, theme } = props;
  const scoreArray = questions && answers && calcScoreArray(questions, answers); // calculate answer match array if questions and answeres received
  const score = questions && answers && calcScore(questions, answers); // calculate score if questions and answeres received

  // redirect to home if no question is in state
  React.useEffect(() => {
    if (!questions) {
      history.push("/");
    }
  }, [questions]);

  return questions && answers ? (
    <>
      <h1 className="header" data-testid="score-header">
        You scored
        <br />
        {score} / {questions.length}
      </h1>
      {questions.map((q, idx) => (
        <Card
          bg={theme.toLowerCase()}
          text={theme.toLowerCase() === "light" ? "dark" : "white"} // opposite of theme
          key={`question-${idx}-card`}
          className="card"
          data-testid={`question-${idx}-card`}
        >
          <Card.Body className="question">
            <Badge
              data-testid={`question-${idx}-badge`}
              className="badge"
              bg={scoreArray[idx] ? "success" : "danger"}
            >
              {scoreArray[idx] ? "+" : "-"}
            </Badge>
            {escapeString(q.question)}{" "}
            {!scoreArray[idx] // display correct answer if answer is wrong
              ? `Correct answer: ${questions[idx].correct_answer}`
              : null}
          </Card.Body>
        </Card>
      ))}
      <div className="button-box">
        <Button onClick={() => playAgain()}>PLAY AGAIN?</Button>
      </div>
    </>
  ) : null;
};

const mapStateToProps = (state) => ({
  questions: state.questions,
  answers: state.answers,
  theme: state.theme,
});

const mapDispatchToProps = (dispatch) => ({
  playAgain: () => dispatch(actions.playAgain()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Results);
