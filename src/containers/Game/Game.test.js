import { render, fireEvent } from "@testing-library/react";
import { createStore } from "redux";
import { Provider } from "react-redux";
import Game from "./Game";

const startingState = {
  currentQuestion: 0,
  questions: [
    { question: "baz", category: "foo category", correct_answer: "True" },
    { question: "bar", category: "bar category", correct_answer: "True" },
  ],
  answers: [true, false],
  theme: "light",
};

const reducer = (state = startingState, action) => {
  switch (action.type) {
    case "SET_CURRENT_QUESTION":
      return {
        ...state,
        currentQuestion: state.currentQuestion + 1,
      };
    default:
      return state;
  }
};

const renderWithRedux = (
  component,
  { initialState, store = createStore(reducer, initialState) } = {}
) => ({
  ...render(<Provider store={store}>{component}</Provider>),
});

it("can render with redux", () => {
  const { getByTestId } = renderWithRedux(<Game />);

  const category = getByTestId("category");
  expect(category).toHaveTextContent("foo category");

  const currentQuestion = getByTestId("current");
  expect(currentQuestion).toHaveTextContent("1 of 2");

  const trueButton = getByTestId("radio-true");
  fireEvent.click(trueButton);

  expect(category).toHaveTextContent("bar category");
  expect(currentQuestion).toHaveTextContent("2 of 2");
});
