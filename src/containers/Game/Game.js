import React from "react";
import { connect } from "react-redux";
import Question from "../../components/Question/Question";
import { escapeString } from "../../utils/utils";
import * as actions from "../../redux/actions";
import "./style.css";

const Game = (props) => {
  const {
    currentQuestion,
    loading,
    questions,
    submitAnswer,
    answers,
    setCurrentQuestion,
    history, // history for redirect once all questions are answered or no question loaded
    theme, // theme must be passed to cards to explicitly set theme
  } = props;

  // if no question is fetched -> redirect to home and fetch when new trivia started
  React.useEffect(() => {
    if (!questions && !loading) {
      history.push("/");
    }
  }, [loading, questions]);

  // if last question answered redirect to results
  React.useEffect(() => {
    if (questions && questions.length === currentQuestion) {
      history.push("/results");
    }
  }, [currentQuestion]);

  return (
    <>
      {questions && currentQuestion + 1 <= questions.length ? ( // don't display question if undefined or last one is answered
        <>
          <h1 className="header" data-testid="category">
            {escapeString(questions[currentQuestion].category)}
          </h1>
          <Question
            question={questions && questions[currentQuestion].question}
            currentQuestion={currentQuestion}
            lengthQuestions={questions.length}
            answer={answers[currentQuestion]}
            submitAnswer={(answer) => {
              submitAnswer(currentQuestion, answer); // set answer
              setCurrentQuestion(currentQuestion + 1); // step question
            }}
            theme={theme}
          />
          <div className="header" data-testid="current">
            {currentQuestion + 1} of {questions.length}
          </div>
        </>
      ) : null}
    </>
  );
};

const mapStateToProps = (state) => ({
  currentQuestion: state.currentQuestion,
  questions: state.questions,
  loading: state.fetchLoading,
  answers: state.answers,
  theme: state.theme,
});

const mapDispatchToProps = (dispatch) => ({
  fetchQuestions: () => dispatch(actions.fetchQuestions()),
  submitAnswer: (currentQuestion, answer) =>
    dispatch(actions.submitAnswer(currentQuestion, answer)),
  setCurrentQuestion: (questionId) =>
    dispatch(actions.setCurrentQuestion(questionId)),
  playAgain: () => dispatch(actions.playAgain()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Game);
