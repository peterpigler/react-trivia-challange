import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import Home from "./Home";

const mockStore = configureMockStore();
const store = mockStore({});

test("renders Trivia Challenge page", () => {
  const { getByText } = render(
    <Provider store={store}>
      <Home />
    </Provider>
  );
  const buttonElement = getByText("BEGIN");
  const headerElement = getByText(/trivia challenge/i);
  expect(buttonElement).toBeInTheDocument();
  expect(headerElement).toBeInTheDocument();
});
