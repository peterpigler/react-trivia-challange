import React from "react";
import { Button } from "react-bootstrap";
import { connect } from "react-redux";
import * as actions from "../../redux/actions";
import "./style.css";

const Home = (props) => {
  const { history, fetchQuestions } = props;

  return (
    <div className="home">
      <h1 className="header" id="welcome">
        Welcome to the Trivia Challenge!
      </h1>
      <h2>You will be presented with 10 True or False questions.</h2>

      <h2>Can you score 100%?</h2>

      <Button
        onClick={() => {
          // fetch questions
          fetchQuestions();

          // redirect to game
          history.push("/play");
        }}
      >
        BEGIN
      </Button>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => ({
  fetchQuestions: () => dispatch(actions.fetchQuestions()),
});

export default connect(null, mapDispatchToProps)(Home);
