import React from "react";
import { ToggleButtonGroup, ToggleButton, Card } from "react-bootstrap";
import { escapeString } from "../../utils/utils";
import "./style.css";

const Question = (props) => {
  const { question, submitAnswer, answer, theme } = props;
  return (
    <>
      <Card
        bg={theme.toLowerCase()}
        text={theme.toLowerCase() === "light" ? "dark" : "white"}
        className="text-center card-style"
      >
        <Card.Body className="card-body">
          <div data-testid="question" className="question-card">
            {escapeString(question)}
          </div>
        </Card.Body>
        <Card.Footer>
          <ToggleButtonGroup
            data-testid="button-group"
            type="radio-group"
            name="choice"
          >
            <ToggleButton
              type="radio"
              id="radio-true"
              data-testid="radio-true"
              variant={`${answer ? "" : "outline-"}primary`}
              checked={answer}
              onClick={() => submitAnswer(true)}
            >
              True
            </ToggleButton>
            <ToggleButton
              variant={`${answer === false ? "" : "outline-"}danger`}
              type="radio"
              id="radio-false"
              data-testid="radio-false"
              checked={answer === false}
              onClick={() => submitAnswer(false)}
            >
              False
            </ToggleButton>
          </ToggleButtonGroup>
        </Card.Footer>
      </Card>
    </>
  );
};

export default Question;
