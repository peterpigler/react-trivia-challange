import { render } from "@testing-library/react";
import Question from "./Question";

test("question renders", () => {
  const { getByTestId, getByText } = render(
    <Question question="Hello world" theme="light" />
  );

  const questionElem = getByText("Hello world");
  const buttonGroup = getByTestId("button-group");
  expect(questionElem).toBeInTheDocument();
  expect(buttonGroup).toBeInTheDocument();
});
