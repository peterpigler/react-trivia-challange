import axios from "axios";

const apiURL =
  "https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean";

export const fetchQuestions = () => axios.get(apiURL);
