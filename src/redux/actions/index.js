export {
  fetchQuestions,
  submitAnswer,
  setCurrentQuestion,
  playAgain,
  changeTheme,
} from "./triviaActions";
