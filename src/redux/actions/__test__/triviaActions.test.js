import { cleanup } from "@testing-library/react";
import axios from "axios";
import * as actionTypes from "../actionTypes";
import reducer, { initialState } from "../../reducers/trivia";
import * as utils from "../utils";

afterEach(cleanup);

jest.mock("axios");

it("should fetch questions", () => {
  const questions = [
    { question: "foo", category: "bar", correct_answer: "True" },
  ];
  const resp = {
    data: { results: questions },
  };
  axios.get.mockResolvedValue(resp);
  return utils
    .fetchQuestions()
    .then((fetchedData) => expect(fetchedData.data.results).toEqual(questions));
});

it("should return the inital state", () => {
  expect(reducer(undefined, {})).toEqual(initialState);
});

it("should go to next question", () => {
  expect(
    reducer(initialState, {
      type: actionTypes.SET_CURRENT_QUESTION,
      questionId: 2,
    }).currentQuestion
  ).toEqual(2);
});

it("should start fetching questions", () => {
  expect(
    reducer(initialState, { type: actionTypes.FETCH_QUESTIONS_START })
      .fetchLoading
  ).toEqual(true);
});

it("should populate questions", () => {
  const matchObject = {
    ...initialState,
    fetchLoading: false,
    questions: ["foo", "bar", "baz"],
    currentQuestion: 0,
    answers: [null, null, null],
  };
  expect(
    reducer(initialState, {
      type: actionTypes.FETCH_QUESTIONS_SUCCESS,
      questions: ["foo", "bar", "baz"],
    })
  ).toMatchObject(matchObject);
});

it("should reset game", () => {
  const matchObject = {
    ...initialState,
    currentQuestion: null,
    answers: null,
    questions: null,
  };
  expect(
    reducer(initialState, {
      type: actionTypes.PLAY_AGAIN,
    })
  ).toMatchObject(matchObject);
});
