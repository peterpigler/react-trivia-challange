import * as actionTypes from "./actionTypes";
import * as utils from "./utils";

const fetchQuestionsStart = () => ({
  type: actionTypes.FETCH_QUESTIONS_START,
});

const fetchQuestionsSuccess = (questions) => ({
  type: actionTypes.FETCH_QUESTIONS_SUCCESS,
  questions,
});

const fetchQuestionsFail = (error) => ({
  type: actionTypes.FETCH_QUESTIONS_FAIL,
  error,
});

export const fetchQuestions = () => (dispatch) => {
  dispatch(fetchQuestionsStart());
  utils
    .fetchQuestions()
    .then((response) => {
      dispatch(fetchQuestionsSuccess(response.data.results));
    })
    .catch((error) => dispatch(fetchQuestionsFail(error)));
};

export const submitAnswer = (questionId, answer) => ({
  type: actionTypes.SUBMIT_ANSWER,
  questionId,
  answer,
});

export const setCurrentQuestion = (questionId) => ({
  type: actionTypes.SET_CURRENT_QUESTION,
  questionId,
});

export const playAgain = () => ({
  type: actionTypes.PLAY_AGAIN,
});

export const changeTheme = (theme) => ({
  type: actionTypes.CHANGE_THEME,
  theme,
});
