import * as actionTypes from "../actions/actionTypes";

export const initialState = {
  fetchLoading: false,
  error: null,
  questions: null,
  currentQuestion: null,
  answers: null,
  theme: "light",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_QUESTIONS_START:
      return { ...state, fetchLoading: true };
    case actionTypes.FETCH_QUESTIONS_SUCCESS:
      return {
        ...state,
        fetchLoading: false,
        questions: action.questions,
        currentQuestion: 0,
        answers: Array(action.questions.length).fill(null), // create array of answers with length equal to question length
      };
    case actionTypes.FETCH_QUESTIONS_FAIL:
      return {
        ...state,
        fetchLoading: false,
        error: action.error,
      };
    case actionTypes.SUBMIT_ANSWER:
      return {
        ...state,
        answers: [
          // decouple array
          ...state.answers.slice(0, action.questionId),
          action.answer,
          ...state.answers.slice(action.questionId + 1),
        ],
      };
    case actionTypes.SET_CURRENT_QUESTION:
      return {
        ...state,
        currentQuestion: action.questionId,
      };
    case actionTypes.PLAY_AGAIN:
      return {
        ...state,
        currentQuestion: null,
        answers: null,
        questions: null,
      };
    case actionTypes.CHANGE_THEME:
      return {
        ...state,
        theme: action.theme,
      };
    default:
      return state;
  }
};

export default reducer;
